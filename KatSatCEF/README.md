# KatSat-Reference-DataSet

A references Data Set for Virtual Satellite. 
The Data Set represents a cube sat initially designed by Katharina Roventa.

# Copyright and Licensing

The German Aerospace Center (DLR e.V.) makes available all content in this project ("Content").  Unless otherwise 
indicated below, the Content is provided to you for DLR internal use only! Distribution of this content is not permitted!
